// Copyright Epic Games, Inc. All Rights Reserved.

#include "tpsGameMode.h"
#include "tpsPlayerController.h"
#include "tpsCharacter.h"
#include "UObject/ConstructorHelpers.h"

AtpsGameMode::AtpsGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AtpsPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/Blueprints/Character/BP_TopDownCharacter"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}
// Script / Engine.Blueprint'/Game/Blueprints/Character/BP_TopDownCharacter.BP_TopDownCharacter'